const API = 'https://dummyjson.com/products'
let productsData = []
let itemsPerPage = 12
let currentPage = 1

async function dataTable() {
  await productTable()
  console.log(productsData)

  const pages = []
  for (let i = 0; i <= Math.ceil(productsData.length / itemsPerPage); i++) {
    pages.push(i)
  }

  const indexOfLastPage = currentPage * itemsPerPage;
  const indexOfFirsPage = indexOfLastPage - itemsPerPage;
  const currentItems = productsData.slice(indexOfFirsPage, indexOfLastPage)

  document.querySelector('.card').innerHTML = currentItems.map(product =>
    `
     <div class="card-box">
     <img src=${product.images[0]} alt="">
     <p>${product.category}</p>
     <p>Price ${product.price}$</p>
     <h4>${product.title.slice(0, 18)}...</h4>
     <button onclick="addToCard(${product.id})">Add</button>
     </div>
    `
  ).join("")
}

dataTable()

const prevBtn = () => {
  if ((currentPage - 1) * itemsPerPage) {
    currentPage--;
    dataTable()
  }
}
const nextBtn = () => {
  if ((currentPage * itemsPerPage) / productsData.length) {
    currentPage++;
    dataTable()
  }
}

document.getElementById('prev').addEventListener('click', prevBtn, false)
document.getElementById('next').addEventListener('click', nextBtn, false)

let basket = []

function addToCard(id) {
  if (productsData[id] == null) {
    console.log('hello world')
  } else {
    basket.push(id)
    document.getElementById('basket').innerHTML = `basket ${basket.length}`

  }
  console.log(basket.length)

}

async function productTable() {
  const data = await fetch(API)
  const res = await data.json()
  productsData = res.products
  // console.log(productsData)
}

productTable()